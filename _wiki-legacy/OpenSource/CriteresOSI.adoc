---
title: OpenSource/CriteresOSI
tags: ''
toc: true
created: '2006-04-18'
modified: '2006-04-18'
revisions:
- author: WikiSysop
  date: '2006-04-18'
  comment: Import...
---


Attention, ceci n'est qu'un court résumé des critères. Pour élaborer une licence compatible, il convient de se référer au link:http://www.opensource.org/docs/definition.php[texte original].


== Code source

Le programme doit inclure le code source.


== Redistribution libre et gratuite

La licence ne doit pas limiter le droit de vendre ou de donner le logiciel.


== Travaux dérivés

La licence doit autoriser les modifications et travaux dérivés.


== Autres critères


* Intégrité du code source de l'auteur
* Absence de discrimination envers des personnes ou des groupes
* Absence de discrimination envers des domaines d'activité
* Distribution de licence
* La licence ne doit pas être spécifique à un produit
* La licence ne doit pas restreindre d'autres logiciels
* La licence doit être neutre technologiquement


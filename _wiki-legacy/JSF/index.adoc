---
title: JSF
tags: ''
toc: true
created: '2006-06-15'
modified: '2006-06-15'
author: Olivier
---
Pour définir une variable avec <c:set> dans une page JSF, deux éléments sont à considérer :



* Le scope _page_ n'existe pas avec JSF
* Si la variable est définie à partir d'un backing bean, il convient de s'assurer que le backing bean existe effectivement



*1. Définir le scope dans <c:set>*



Par défaut, _<c:set>_ créé une variable en portée _page_. Il faut donc définir explicitement le scope (_request_, _session_ ou _application_) si vous souhaitez exploiter cette variable dans une EL JSF.



[source.width-80, subs="verbatim,quotes"]
----
<c:set var="d" value="${detailProduitBean}" scope="request"/>

----



*2. Gestion du backing bean*



Les backing beans sont instanciés par le framework JSF (Managed Bean Creation Facility), lorsqu'ils sont accédés pour la première fois (pour un scope donné) dans une EL JSF. Par conséquent, si la balise _<c:set>_ est exécutée avant qu'une EL JSF ne sollicite le backing bean, la variable du _<c:set>_ ne sera pas correctement initialisée. Pour résoudre ce problème, on peut utiliser le _VariableResolver_ de l'application pour s'assurer que le bean en question existe effectivement.



[source.width-80, subs="verbatim,quotes"]
----
FacesContext ctx = FacesContext.getCurrentInstance()
VariableResolver resolver = ctx.getApplication().getVariableResolver();

// création du backing bean par JSF
DetailProduitBean d = (DetailProduitBean)resolver.resolveVariable(ctx, "detailProduitBean");

// initialisation du bean à partir des infos récupérées depuis la couche métier
d.setInfoProduit(service.findInfoProduit());


----

Ce code doit être placé dans un backing bean qui est invoqué avant l'affichage de la page JSF.


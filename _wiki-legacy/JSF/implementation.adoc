---
title: JSF/implementation
tags: ''
toc: true
created: '2006-11-12'
modified: '2007-03-06'
---

JSF est une spécification optionnelle de J2EE 1.4 et intégrée à JavaEE 5.

Plusieurs implémentations du standard sont disponibles, ainsi que des librairies d'extension.


== Composants standard

Ce sont des implémentations de la spécification (JSR-127), avec les composants standard (taglib core et html).

=== Sun JSF RI

* C'est l'implémentation de référence de Sun, disponible dans la toute dernière version de la spécification (fin 2006 = 1.2).

=== Apache MyFaces

* C'est une implémentation très couramment utilisée. La version est généralement en décalage, puisque fin 2006, on n'avait encore que la 1.1.
* Elle est intégrée à JBoss 4.0.
* https://myfaces.apache.org/


== Composants complémentaires

Remarque : 3 librairies chez Apache qui, pour l'instant ont des zones de recouvrement.

=== Apache Tomahawk

* Extension de JSF compatible avec les implémentations standard
* Certains composants remplacent les composants standard
* Support de Struts Tiles

=== Apache Tobago

* Framework JSF intégré, largement incompatible avec les autres extensions (incompatibilité de renderkit)

=== Oracle ADF

* Composants en remplacement ou en ajout du standard
* Plusieurs type de tableaux et de listes

* Validateurs et convertisseur coté client (javascript)
* Support des principes d'accessibilité
* Rendu partiel de page
* http://www.oracle.com/technology/products/adf/

=== Apache Trinidad

* Version open source de ADF Faces
* En cours d'intégration chez Apache


== Composants AJAX

AJAX devrait être totalement intégré à JSF pour la version 2.0 (JavaEE 6). 
En attendant, pour développer des applications et sites au rendu un peu dynamique, nous devons utiliser des extensions.

L'utilisation de frameworks AJAX autonomes de JSF est dangereuse et peu poser des problèmes de gestion de l'état des composants. 
Par ailleurs, quelques librairies se contentent d'une intégration minimale d'AJAX, avec un champ de type "suggest".

Nous avons privilégié la fonction de rendu partiel de page.

=== Ajax4JSF

* Ajouts spécifiquement AJAX
* Facile d'utilisation pour les boutons, plus délicat pour d'autres événements
* Le projet est passé sur la coupe de JBoss
* https://en.wikipedia.org/wiki/Ajax4jsf

=== Rich Client Faces

* Les premiers retours semblent prometteurs...
* Licence open source LGPL
* Développé par la société link:http://www.vedana.com/[Vedana]

=== Sun DynaFaces

* En cours de développement, premières versions early access (fin 2006)
* https://www.oracle.com/technical-resources/articles/javaee/ajax-javaserverfaces.html

=== IceFaces

* Redéfinit les composants standard de JSF, avec des attributs supplémentaires
* Existe en version Open Source ou en version commerciale
* Semble assez riche et bien documenté
* Rendu partiel et validation partielle de formaulaire
* Push : mise à jour de page initiées par le serveur
* Drag & drop et autres effets, grâce à l'intégration de script.aculo.us
* https://icesoft.com/icefaces/

=== Exadel RichFaces / JBoss RichFaces

* Initialement développé sous licence commerciale par la société link:http://www.exadel.com[Exadel]
* Annonce d'une licence Open Source via JBoss.org
* https://richfaces.jboss.org/

---
title: JUnit/Theory
toc: false
created: '2011-01-12'
modified: '2011-01-12'
---
L'annotation link:https://junit.org/junit4/javadoc/4.13/org/junit/experimental/theories/Theory.html[@Theory] est utilisée en remplacement de l'annotation @Test. 
Elle est utilisée pour des méthodes de test avec arguments et utilise les valeurs des champs annotés @DataPoint ou @DataPoints pour créer des combinaisons d'arguments.



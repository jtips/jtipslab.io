---
title: Sessions HTTP
toc: true
author: Alexis
created: '2007-02-22'
modified: '2007-02-22'
---

== Sessions http

Les sessions http servent à stocker des objets qui restent disponibles tout au long de la navigation d'un utilisateur. Une session contient les beans JSF déclarés en scope session, mais aussi d'autres objets qui y auront été mis explicitement. La liaison entre l'utilisateur (ou plutôt son navigateur) et sa session est représentée par une valeur j_session_id, transmise au serveur via un cookie, dans l'URL ou dans le corps d'une requête POST.


=== Sérialisation de session

Les sessions peuvent faire l'objet d'entrées et sorties de la machine virtuelle lorsqu'on met en oeuvre des mécanismes de tolérance de panne ou de répartition de charge. 


Par défaut, Tomcat sauvegarde les sessions dans un fichier sessions.ser afin de les restaurer lorsqu'on redémarre le serveur. Les sessions peuvent aussi transiter par le réseau lorsqu'on met Tomcat en cluster. Dans ces 2 cas, seuls les objets sérialisables pourront être enregistrés dans le fichier ou transmis par le réseau.


La sérialisation dans le fichier sessions.ser a été désactivée dans la configuration du projet :



[source.width-80, subs="verbatim,quotes"]
----
 <Context>
   ...
   <Manager pathname="" />
 </Context>

----

Ces mécanismes imposent 2 bonnes pratiques :



* Alléger la session : on ne place en session que les objets dont on a besoin tout au long de la navigation
* Placer des objets sérialisables : on ne place en session que des objets sérialisables

=== Objets sérialisables

Pour être sérialisable, un objet doit avant tout implémenter l'interface Serializable.
Cependant, cela ne suffit pas, il faut aussi que tous ses champs répondent à au moins un des critères suivants :



* type primitif (int, float,...)
* type sérialisable (String, Integer,..., classe implémentant Serializable)
* transient

Un champ déclaré transient sera exclu de la sérialisation. Il est particulièrement recommandé de déclarer ainsi les champs qui sont trop lourds (texte volumineux) ou les objets sans état.



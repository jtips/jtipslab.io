---
title: WeldSE/Test/Arquillian-Pom
toc: true
redirects:
- WeldSE/Test/index
created: '2011-01-30'
modified: '2011-01-30'
---

// <!--more-->

[source.width-80, subs="verbatim,quotes"]
----
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" 
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
                             http://maven.apache.org/maven-v4_0_0.xsd">
   
   <modelVersion>4.0.0</modelVersion>
   
   <groupId>org.jboss.arquillian.showcase</groupId>
   <artifactId>arquillian-showcase-cdi-ah</artifactId>
   <version>1.0.0-SNAPSHOT</version>
   <packaging>jar</packaging>
   <name>Arquillian Showcase: CDI by AH</name>
   <description>Examples that demonstrate how to test CDI components using Arquillian</description>
   
   <dependencies>
       <dependency>
           <groupId>org.jboss.weld</groupId>
           <artifactId>weld-core</artifactId>
       </dependency>
       <dependency>
           <groupId>org.jboss.weld</groupId>
           <artifactId>weld-api</artifactId>
       </dependency>
       <dependency>
           <groupId>org.jboss.spec</groupId>
           <artifactId>jboss-javaee-6.0</artifactId>
           <version>1.0.0.Final</version>
           <type>pom</type>
       </dependency>
       <dependency>
           <groupId>org.slf4j</groupId>
           <artifactId>slf4j-log4j12</artifactId>
           <version>1.5.10</version>
       </dependency>
       <dependency>
           <groupId>log4j</groupId>
           <artifactId>log4j</artifactId>
           <version>1.2.16</version>
       </dependency>
   
       <dependency>
           <groupId>junit</groupId>
           <artifactId>junit</artifactId>
           <version>4.8.2</version>
           <scope>test</scope>
       </dependency>
       <dependency>
           <groupId>org.jboss.arquillian</groupId>
           <artifactId>arquillian-junit</artifactId>
           <version>1.0.0.Alpha4</version>
           <scope>test</scope>
       </dependency>
       <dependency>
           <groupId>org.jboss.weld.arquillian.container</groupId>
           <artifactId>arquillian-weld-ee-embedded-1.1</artifactId>
           <version>1.1.0.Final</version>
           <scope>test</scope>
       </dependency>
   </dependencies>
   
   <dependencyManagement>
       <dependencies>
           <dependency>
               <groupId>org.jboss.weld</groupId>
               <artifactId>weld-core-bom</artifactId>
               <version>1.1.0.Final</version>
               <type>pom</type>
               <scope>import</scope>
           </dependency>
       </dependencies>
   </dependencyManagement>
   
   <repositories>
       <repository>
           <id>jboss-public-repository</id>
           <name>JBoss Repository</name>
           <url>http://repository.jboss.org/nexus/content/groups/public</url>
           <releases>
               <updatePolicy>never</updatePolicy>
           </releases>
           <snapshots>
               <enabled>false</enabled>
           </snapshots>
       </repository>
   </repositories>
   
</project>
----

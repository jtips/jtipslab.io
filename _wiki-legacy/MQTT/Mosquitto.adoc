---
title: MQTT/Mosquitto
tags: MQTT
toc: true
created: '2014-04-04'
modified: '2014-04-04'
revisions:
- author: Alexis
  date: '2014-04-04'
  comment: ''
---
Mosquitto est le plus connu des brokers MQTT. Il est Open Source et a été transféré dans la fondation Eclipse.




== Installation

Mosquitto peut être link:http://mosquitto.org/download/[installé] sous Windows, Linux ou MacOS.


=== Ubuntu

Sous Ubuntu 13.04, Mosquitto peut directement être installé par apt-get, sans ajouter de repository supplémentaire :



[source.width-80, subs="verbatim,quotes"]
----
sudo apt-get install mosquitto

----

Mosquitto est installé en service. Il est automatiquement démarré au lancement de la machine.


Dans ce cas, le fichier de configuration est /etc/mosquitto/mosquitto.conf


=== MacOS

Sous MacOS, j'ai utilisé homebrew :



[source.width-80, subs="verbatim,quotes"]
----
brew install mosquitto

----

Mosquitto n'est pas automatiquement installé en service, mais comme Homebrew est plutôt bien fait (pas pour tout, mais là oui), il nous explique comment le faire :



[source.width-80, subs="verbatim,quotes"]
----
ln -sfv /usr/local/opt/mosquitto/*.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mosquitto.plist

----

Pour démarrer Mosquitto en mode console sans service : 



[source.width-80, subs="verbatim,quotes"]
----
mosquitto -d -c /usr/local/etc/mosquitto/mosquitto.conf

----

Dans ce cas, le fichier de configuration est /usr/local/etc/mosquitto/mosquitto.conf. Si on ne précise pas le fichier de configuration avec l'option -c, aucun fichier n'est utilisé et ce sont les valeurs par défaut qui sont utilisées.


=== Windows

Sous Windows, je ne l'ai jamais installé, et je ne le ferai que sous la contrainte.


== Configuration

La configuration se fait dans un fichier .conf qu'on spécifie au lancement de mosquitto (cf. ci-dessus).



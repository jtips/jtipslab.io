---
title: Intégration Java / PHP
tags: ''
toc: true
author: Alexis
created: 2008-09-05
modified: 2008-09-05
---

Quelles sont les solutions pour mettre en place une intégration entre PHP et Java ?

Ces notes ont servi de base pour la rédaction d'un article paru le numéro 6/2008 de link:http://www.phpsolmag.org[PHP Solutions] de novembre 2008.


== Objectif d'intégration

Pourquoi vouloir intégrer Java et PHP ?

=== Intégrer un progiciel

Pour intégrer un progiciel écrit en PHP dans un environnement Java, il faut développer des modules dans un langage, capables d'interagir avec l'autre langage.

=== Fusionner deux systèmes d'information

Lors de la fusion de 2 sociétés, un système d'information peut être choisi au dépend de l'autre, ou les deux systèmes peuvent être fusionnés. 
Dans ce cas, il est peut probable que les langages choisis initialement soit les mêmes. 
Il faut donc mettre en place des modules d'intégration capable de s'adresser aux logiciels, quels que soient leurs langages.

=== Application multi-langages

Les progrès des techniques d'interopérabilité permettent aujourd'hui de concevoir des architectures multi-langages dont l'objectif n'est plus d'intégrer un existant, mais d'exploiter chaque langage pour ce à quoi il est le plus apte : 

* PHP pour les pages Web
* Java pour les couches métier (avec Spring ou EJB) et pour la persistence (avec JPA ou Hibernate)

image::/img/php/PhpJavaCouche.png[PhpJavaCouche.png, 600, 195" srcset="]


== Solutions

=== WebService : SOAP et REST

Les solutions à base de Services Web sont adaptées à la plupart des langages modernes : PHP, Java, C# et .NET, C++,... 
Elles sont souvent utilisées dans les architectures orientée services (SOA).

SOAP est le protocole standard pour ce type de solution. 
REST est une altenative moins standard, mais plus simple.

image::/img/php/PhpJavaSoap.png[PhpJavaSoap.png, 600, 246" srcset="]

=== Pont Java-PHP

La communication entre Java et PHP s'appuie partiellement sur les mêmes techniques que pour les Web Services : HTTP et XML. 
Par contre, les flux XML sont plus compacts et une partie des communications passe par des sockets de plus bas niveau.

Il existe deux solutions sur le marché :

* celle de Zend Technologies, vendue sous licence commerciale avec le Zend Platform ES, 
* link:/PHP/Bridge[PHP/Java Bridge], une solution open source, sous licence LGPL, hébergée sur la plate-forme SourceForge.net

image::/img/php/PhpJavaBridge.png[PhpJavaBridge.png, 600, 246" srcset="]

=== Intégration in-process

Plutôt que d'établir un pont entre deux processus, l'intégration in-process permet d'embarquer du PHP dans une machine virtuelle Java. 
En fait, le moteur PHP traditionnel est remplacé par un moteur écrit en Java.

Il existe deux solutions sur le marché :

* link:/PHP/Quercus[Caucho Quercus] est un moteur PHP5 développé en Java et distribué sous licence GPL. 
* link:Project Zero, le projet open source initié par IBM, et qui sert de base aux développements de WebSphere sMash, propose des fonctionnalités du même type.

image::/img/php/PhpJavaQuercus.png[PhpJavaQuercus.png, 600, 246" srcset="]

== Quel choix ?

* Services : environnements multi-langages, avec des besoins d'intégration importants et architectures orientées services
* Pont : appels simples de classes Java depuis des pages PHP, dans le cadre de développements d'applications mixtes ou de modules d'intégration en PHP
* In-process : applications mixtes PHP / Java, mais manque de recul sur la solution

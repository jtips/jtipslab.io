---
title: PHP/QuercusTomcat
tags: ''
toc: true
created: '2008-12-30'
modified: '2008-12-30'
revisions:
- author: Alexis
  date: '2008-12-30'
  comment: ''
---

Apache Tomcat est un serveur d'applications Web plus répandu que Resin. 
Il est donc intéressant de tester l'utilisation de Quercus avec Tomcat.


== Installation

Pour démarrer, il faut un environnement Tomcat qui fonctionne, avec Tomcat 5 ou 6 et un JDK 5 ou 6. 
Il faut aussi télécharger Quercus Standalone sur le site de Caucho.

Pour installer Quercus, il faut décompresser le fichier war téléchargé (quercus-3.2.1.war) sous forme de répertoire (quercus, par exemple), puis copier ce répertoire dans répertoire webapps de Tomcat. 
Les pages PHP pourront être ajoutées dans ce répertoire ; elles seront appelée par l'URL `\http://localhost:8080/quercus/nom-de-la-page.php`.

Cette procédure peut s'appliquer à n'importe quel serveur d'applications qui supporte le déploiement en mode répertoire (JBoss, Glassfish,...). 
Pour les serveurs qui imposent le mode archive, il faut décompresser l'archive de Quercus, ajouter les fichiers php, puis reconstituer l'archive.


== Ajout de PHP dans une application

Pour ajouter le support de PHP à une application existante ou en cours de développement, il faut surtout ajouter la servlet « Quercus Servlet ».
Pour cela, il faut prendre les fichiers quercus.jar et resin-util.jar dans le répertoire WEB-INF/lib de l'archive et de les ajouter dans le même répertoire de notre application. Ensuite, il faut déclarer la servlet dans le fichier web.xml du répertoire WEB-INF. 
Enfin, dans le même fichier, il faut déclarer la prise en charge des fichiers php par cette servlet.

[source.width-80, subs="verbatim,quotes"]
----
 <servlet>
   <servlet-name>Quercus Servlet</servlet-name>
   <servlet-class>com.caucho.quercus.servlet.QuercusServlet</servlet-class>
 </servlet>
 <servlet-mapping>
   <servlet-name>Quercus Servlet</servlet-name>
   <url-pattern>*.php</url-pattern>
 </servlet-mapping>

----


== Ajout de PHP dans toutes les applications

Dans Resin, toutes les applications supportent PHP. Pour avoir le même comportement dans Tomcat, il faut placer les fichiers quercus.jar et resin-util.jar dans le répertoire lib de Tomcat, comme pour la procédure précédente. 
La différence, c'est que nous n'allons déclarer la servlet de Quercus qu'une seule fois, au niveau global, dans le fichier web.xml du répertoire conf de Tomcat.

Attention, dans le cas d'une déclaration globale, il ne faut plus déclarer localement la servlet car cela créerait une conflit.

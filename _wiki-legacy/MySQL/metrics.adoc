---
title: MySQL/metrics
tags: ''
toc: true
archive: false
created: '2013-11-19'
modified: '2013-11-19'
---
Métriques sur les tables : nombre de lignes, taille de données et des indexes.



[source.width-80, subs="verbatim,quotes"]
----
SELECT concat(table_schema,'.',table_name), 
       table_rows Rows, 
       round(data_length/(1024*1024),2) "Data (MB)", 
       round(index_length/(1024*1024),2) "Index (MB)", 
       round((data_length+index_length)/(1024*1024),2) "Total size (MB)"
FROM information_schema.TABLES 
ORDER BY data_length+index_length DESC;

----

Cumul



[source.width-80, subs="verbatim,quotes"]
----
SELECT table_schema, 
       round(sum(data_length)/(1024*1024),0) "Data (MB)", 
       round(sum(index_length)/(1024*1024),0) "Index (MB)", 
       round(sum(data_length+index_length)/(1024*1024),0) "Total size (MB)"
FROM information_schema.tables 
GROUP BY table_schema;

----

Pour avoir le nombre de requêtes total ou par seconde :



[source.width-80, subs="verbatim,quotes"]
----
mysql> status

----

Renvoie



[source.width-80, subs="verbatim,quotes"]
----
Threads: 14  Questions: 65212688  Slow queries: 26  Opens: 10849  Flush tables: 159  Open tables: 60  Queries per second avg: 15.048

----

Autre solution, avec une requête :



[source.width-80, subs="verbatim,quotes"]
----
SELECT uptime.variable_value, 
       queries.variable_value, 
       round(queries.variable_value/uptime.variable_value,2) 
FROM information_schema.SESSION_STATUS as queries, 
     information_schema.SESSION_STATUS as uptime 
WHERE uptime.variable_name='UPTIME' 
  AND queries.variable_name='QUERIES';

----


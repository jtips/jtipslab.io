---
title: Installer et configurer mod_cluster
tags: WildFly Apache
toc: true
links:
- url: https://www.sewatech.fr/formation-wildfly.html
  text: Administration WildFly
author: Alexis
created: '2015-04-25'
modified: '2015-04-25'
---

== Installation (Apache Web Server 2.2)

Télécharger mod_cluster sur link:http://mod-cluster.jboss.org/downloads/[http://mod-cluster.jboss.org/downloads/]

Sur la page de téléchargement, sont mélangés trois types de distributions : 

* java bundles : inutile pour WildFly puisque cette partie y est déjà intégrée
* mod_cluster native bundles with httpd : c'est un Apache Web Server avec le module mod_cluster intégré ; pourquoi pas mais ce n'est pas le sujet ici
* mod_cluster modules for httpd : c'est le module mod_cluster dans sa plus simple forme

Nous allons donc utiliser le module mod_cluster que nous intégrerons dans un Apache Web Server déjà installé. Avant de télécharger, vérifiez bien lequel correspond à votre couple OS / httpd ; pour mod_cluster 12.x, seul Apache Web Server 2.2 est supporté ; ça ne fonctionne pas avec Apache 2.4 (testé avec mod_cluster 1.2.6) . L'archive contient 4 fichiers .so qu'on copie dans le répertoire des modules de Apache Web Server.

Il faut charger les quatre modules (fichier httpd.conf) :

[source.width-80, subs="verbatim,quotes"]
----
LoadModule slotmem_module modules/mod_slotmem.so
LoadModule manager_module modules/mod_manager.so
LoadModule proxy_cluster_module modules/mod_proxy_cluster.so
LoadModule advertise_module modules/mod_advertise.so
----

De plus, mod_proxy doit être activé, avec le transport AJP :

[source.width-80, subs="verbatim,quotes"]
----
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_ajp_module modules/mod_proxy_ajp.so
----


== Installation (Apache Web Server 2.4)

Les distributions binaires de mod_cluster 1.2 fonctionnent uniquement avec Apache Web Server 2.2. Si vous voulez utiliser Apache Web Server 2.4, vous devrez compiler mod_cluster vous-même. L'exemple ci-dessous fonctionne avec Ubuntu 14.04.

On commence par installer Apache et quelques prérequis :

[source.width-80, subs="verbatim,quotes"]
----
apt-get update
apt-get install -y autoconf libtool apache2 git
apt-get remove apache2-threaded-dev
apt-get install apache2-prefork-dev
----

Puis on récupère le code source :

[source.width-80, subs="verbatim,quotes"]
----
git clone link:https://github.com/modcluster/mod_cluster.git[https://github.com/modcluster/mod_cluster.git]
cd mod_cluster
----

Puis on compile les modules un à un :

[source.width-80, subs="verbatim,quotes"]
----
cd native/advertise
./buildconf; ./configure --with-apxs=/usr/bin/apxs; make; cp *.so /usr/lib/apache2/modules/
echo "LoadModule advertise_module /usr/lib/apache2/modules/mod_advertise.so" >> /etc/apache2/mods-available/proxy_cluster.load
----

[source.width-80, subs="verbatim,quotes"]
----
cd ../manager
./buildconf; ./configure --with-apxs=/usr/bin/apxs; make; cp *.so /usr/lib/apache2/modules/ 
echo "LoadModule manager_module /usr/lib/apache2/modules/mod_manager.so" >> /etc/apache2/mods-available/proxy_cluster.load
----

[source.width-80, subs="verbatim,quotes"]
----
cd ../mod_proxy_cluster
./buildconf; ./configure --with-apxs=/usr/bin/apxs; make; cp *.so /usr/lib/apache2/modules/ 
echo "LoadModule proxy_cluster_module /usr/lib/apache2/modules/mod_proxy_cluster.so" >> /etc/apache2/mods-available/proxy_cluster.load
----

[source.width-80, subs="verbatim,quotes"]
----
cd ../mod_cluster_slotmem
./buildconf; ./configure --with-apxs=/usr/bin/apxs; make; cp *.so /usr/lib/apache2/modules/ 
echo "LoadModule slotmem_module /usr/lib/apache2/modules/mod_cluster_slotmem.so" >> /etc/apache2/mods-available/proxy_cluster.load
----

On charge les modules :

[source.width-80, subs="verbatim,quotes"]
----
ln -s /etc/apache2/mods-available/proxy.load /etc/apache2/mods-enabled/
ln -s /etc/apache2/mods-available/proxy_ajp.load /etc/apache2/mods-enabled/
ln -s /etc/apache2/mods-available/proxy_cluster.load /etc/apache2/mods-enabled/
----

Et comme avec Apache 2.2, mod_proxy doit être activé, avec le transport AJP :

[source.width-80, subs="verbatim,quotes"]
----
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_ajp_module modules/mod_proxy_ajp.so
----


== Configuration

Le but est de faire communiquer le mod_cluster avec WildFly. On va le faire sur le port 6666. Dans l'exemple ci-dessous, WildFly et Apache Web Server sont sur la même machine et communiquent en localhost.

On va placer la configuration dans un fichier httpd-cluster.conf, qui devra être en include dans le fichier de configuration principal httpd.conf.

[source.width-80, subs="verbatim,quotes"]
----
Listen 127.0.0.1:6666
<VirtualHost 127.0.0.1:6666>
 
  <Location />
    Order deny,allow
    Deny from all
    Allow from 127.0.0.1
  </Location>
 
  KeepAliveTimeout 60
  MaxKeepAliveRequests 0
  
  ManagerBalancerName mycluster
  ServerAdvertise On
  EnableMCPMReceive
  
</VirtualHost>
MemManagerFile /var/log/apache2/
----

La dernière ligne est utile dans certaines configuration d'Apache pour le stockage de certains fichiers, comme la mémoire partagée ou de locks. Par défaut, ces fichiers sont placés dans $server_root/logs/ ; par exemple, sur Ubuntu 14.04, Apache ne démarre pas parce que ce répertoire est /etc/apache2/logs/ et qu'il n'existe pas.

Activer le gestionnaire (pour Apache 2.2)

[source.width-80, subs="verbatim,quotes"]
----
<Location /cluster-manager>
  SetHandler mod_cluster-manager
  Order deny,allow
  Deny from all
  Allow from 127.0.0.1
</Location>
----

Activer le gestionnaire (pour Apache 2.4)

[source.width-80, subs="verbatim,quotes"]
----
<Location /cluster-manager>
  SetHandler mod_cluster-manager
  Require ip 127.0.0.1
</Location>
----

== WildFly

Le module mod_cluster peut être utilisé avec JBoss AS, en version 7 ou plus ancienne, ou avec Tomcat. Avec WildFly 8, la configuration est très simple. Il suffit de le lancer dans un profil HA, avec un binding réseau plus large que celui par défaut. En standalone, on peut le lancer comme ça :

[source.width-80, subs="verbatim,quotes"]
----
bin/standalone.sh -c standalone-ha.xml -b 0.0.0.0
----

== Vérifications

Si tout va bien, on devrait voir apparaître une trace de ce type dans les logs de WildFly :

[source.width-80, subs="verbatim,quotes"]
----
08:21:56,960 INFO  [org.jboss.modcluster] (UndertowEventHandlerAdapter - 1) MODCLUSTER000012: default-server connector will use /127.0.0.1
----

Et en consultant la page du cluster-manager (`\http://localhost/cluster-manager`), on devrait voir apparaître le node dans la liste.

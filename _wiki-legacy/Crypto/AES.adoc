---
title: Crypto/AES
toc: true
created: '2015-07-17'
modified: '2015-07-17'
---

Générer une clé : 



[source.width-80, subs="verbatim,quotes"]
----
       SecretKey key = KeyGenerator.getInstance("AES").generateKey();

----

Lire une clé (texte) :



[source.width-80, subs="verbatim,quotes"]
----
       byte[] keyAsBytes = "0123456789ABCDEF".getBytes();
       SecretKey key = new SecretKeySpec(keyAsBytes, 0, keyAsBytes.length, "AES");

----

Chiffrer du texte :



[source.width-80, subs="verbatim,quotes"]
----
       String text = "Hello"
       Cipher aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
       aesCipher.init(Cipher.ENCRYPT_MODE, key);
       byte[] cipherBytes = aesCipher.doFinal(text.getBytes());

----

Déchiffrer :



[source.width-80, subs="verbatim,quotes"]
----
       Cipher aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
       aesCipher.init(Cipher.DECRYPT_MODE, key);
       String text = new String(aesCipher.doFinal(cipherBytes));

----


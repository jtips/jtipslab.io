---
title: Installation JBoss AS 4
tags: JBoss
toc: true
links:
- url: https://www.sewatech.fr/formation-jboss4.html
  text: Administration JBoss
author: Alexis
created: '2006-04-18'
modified: '2010-06-29'
---


La procédure décrite ci-dessous s'applique pour JBoss 4.x




== Pré-requis (JBoss 4.0)

=== Système


* Windows
* Unix, Linux
* Mac OS X

Il semblerait qu'on puisse installer JBoss sur un IBM iSeries (AS/400), mais je n'ai jamais fait le test.


=== Machine virtuelle


* JSE 5.0 (JDK ou JRE)
* J2SE 1.4 (JDK ou JRE)

=== Téléchargement

* link:http://www.jboss.org/jbossas/downloads/[http://www.jboss.org/jbossas/downloads/]

OU directement sur sourceforge


* link:http://sourceforge.net/projects/jboss[http://sourceforge.net/projects/jboss]

Fichiers :



* jboss-4.0.x.zip / jboss-4.0.x.tar.gz / jboss-4.0.x.tar.bz2
* Installation

* jboss-4.0.x-src.zip / jboss-4.0.x-src.tar.gz / jboss-4.0.x-src.tar.bz2
* Code source

* jbas-xxxx-patch.zip
* Patch (tous systèmes)


== Pré-requis (JBoss 4.2)

=== Machine virtuelle

JBoss 4.2 fonctionne normalement avec le JDK 5. Cependant, pour la version 4.2.3, une distribution est prévue pour le JDK 6.


=== Téléchargement

On retrouve des fichiers pour les versions installables, ainsi que pour le code source. Par contre, il n'y a plus de patch.



* jboss-4.2.3.GA.zip
* Installation avec JavaSE 5

* jboss-4.2.3.GA-jdk6.zip
* Installation avec JavaSE 6

* jboss-4.2.3.GA-src.tar.gz  
* Code source


Les fichiers MD5 et SHA permettent de valider l'intégrité des fichiers téléchargés.


== Installation

=== Installation simple

Décompresser l'archive !


Pour tester l'installation, lancer le script bin/run.bat ou bin/run.sh. La variable JAVA_HOME doit pointer sur le répertoire du JDK.


La console dos ou shell doit afficher ceci :



[source.width-80, subs="verbatim,quotes"]
----
==========================================================================
  JBoss Bootstrap Environment
  JBOSS_HOME: ...\bin\\..
  JAVA: ...
  JAVA_OPTS:  -Dprogram.name=run.bat -Xms128m -Xmx512m
  CLASSPATH: ...\lib\tools.jar;...\bin\\run.jar
==========================================================================
17:29:33,852 INFO  [Server] Starting JBoss (MX MicroKernel)...
...
...
...
17:29:50,243 INFO  [Server] JBoss (MX MicroKernel) [4.0.2 (build:
     CVSTag=JBoss_4_0_2 date=200505022023)] Started in 16s:375ms

----

Un second test consiste à lancer la page d'accueil depuis le navigateur web, sur le port 8080.


Pour arrêter JBoss, on peut utiliser le script shutdown.sh, ou envoyer un kill au processus. En mixant kill et jps, on peut avoir une commande générique comme celle-ci.



[source.width-80, subs="verbatim,quotes"]
----
kill `jps -l | grep org.jboss.Main | awk '{ print $1 }'`

----

=== Installation en service Linux

Une fois JBoss installé, il peut être pratique de l'installer en service, afin qu'il se lance sans intervention humaine, au démarrage du serveur. JBoss fournit des scripts types, pour RedHat, Suse et HP-UX. Il semblerait que le script RedHat fonctionne sur Ubuntu.


Cependant, j'ai préféré refaire un script, simplifié, pour mon serveur préféré. Un des raisons à cela, est que le script fourni en standard ne fonctionne que si JMX et twiddle ne sont pas sécurisés !!! Je préfère donc arrêter mon JBoss par un _kill -TERM_.






== Script d'installation

Même si elle est relativement simple, cette installation peut être automatisée par script, sous Linux. Celui-ci peut alors aussi prendre en charge automatiquement l'installation en service. 


Le link:http://download.jtips.info/scripts/jboss-install.sh[script] que j'ai conçu a été testé pour une installation de *JBoss 5.1*, avec *JavaSE 6*, sur un serveur *Ubuntu Server 9.04*. 


Une partie du script, en particulier pour l'installation de Java est commune avec l'link:/Tomcat/Installation[installation par script de Tomcat]. Pour le reste, les principales différences sont :



* Téléchargement depuis Sourceforge
* Pas de modification du système de traces


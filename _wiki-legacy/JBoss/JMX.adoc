---
title: Monitoring JMX dans JBoss 4 et 5
tags: JBoss
toc: true
links:
- url: https://www.sewatech.fr/formation-jboss4.html
  text: Administration JBoss 4
- url: https://www.sewatech.fr/formation-jboss-5.html
  text: Administration JBoss 5
author: Alexis
created: 2006-04-18
modified: 2010-07-27
---

JMX est le protocole universel dans JBoss 4. Tous les composants déployés dans le serveur d'application peuvent être intérrogés par ce protocole. 
Il est possible de récupérer les valeurs de leurs attributs et d'invoquer des opérations.

JMX est aussi, et avant tout, le protocole standard de monitoring des applications Java.


== Consoles JBoss

=== Console JMX

La plus ancienne façon d'accéder aux composants JMX de JBoss est l'application jmx-console (`\http://localhost:8080/jmx-console`).

Les premières informations inspectées habituellement sont dans les MBeans suivants :

* `jboss:service:JNDIView` => permet de parcourir l'annuaire JNDI
* `jboss.system:type=ServerInfo` => permet de suivre les informations de mémoire

=== Console Web

Les informations JMX sont classées dans l'arborescence « System / JMX MBeans ». 
Elles sont ensuite classées par catégorie, comme dans la jmx-console.

Les informations générales du serveur sont affichées sur la page d'accueil et peuvent être retrouvées dans le MBean `jboss.system:type=ServerInfo`.

Les applications déployées sont classées dans l'arborescence "J2EE Domains / Manager / JBoss", puis par fichier déployé (.jar, .war, .sar,...). 
Pour les EJB, les statistiques montrent le nombre de create et de remove, ainsi que les temps d'exécution des méthodes. 
Pour les servlet, les statistiques concernent les temps de traitement de requêtes.

Pour suivre l'évolution de la mémoire dans une courbe, il faut se positionner sur le MBean `jboss.system:type=ServerInfo`, attribut `FreeMemory` puis par clic droit sélectionner "graph". 
Cette manipulation peut se faire sur n'importe quel attribut numérique.
Il faut noter que le graphe s'affiche dans une applet.
La fonctionnalité de Snapshot permet de collecter les données et d'afficher une courbe statique.

Pour placer une alerte sur une donnée, on sélectionne "create monitor". 
La valeur limite est placée dans le champs `Threshold`, la fréquence de surveillance est placée dans le champs "Time Period". 
Il ne faut pas oublier d'activer la surveillance en cochant la case "Enable Monitor".
Les recueils de données et les surveillances sont placées respectivement dans les hierarchies "Monitors / Snapshots" et "Monitor / Alerts".
Dans la configuration standard, seule la "Console Alert" est disponible. 
Celle-ci envoie une trace dans Log4J lorsque la valeur limite est franchie. 
Une "Email Alert" peut être activée dans le fichier deploy/monitoring-service.xml de la configuraion.

*Remarques*:

La web-console affiche l'arborescence de ressources dans une applet. 
Il faut donc que le navigateur supporte cette fonctionnalité via le plug-in java.

=== JOPR

link:http://www.jboss.org/jopr/[JOPR] est la version open source de l'outil de monitoring JBoss Operation Network. 
Cette outil peut être considéré comme un concurrent de link:#Outils_de_monitoring_traditionnels[Nagios], puisqu'il permet de surveiller plusieurs types de ressources: système, réseau, java,... 
C'est donc bien plus qu'une console concurrente des deux citées précédemment. 

Par contre, il existe une version allégée, link:http://www.jboss.org/embjopr/[Embedded JOPR], qui se déploie sous forme d'un war et qui peut être utilisée pour certaines tâches d'administration.

Celle-ci permet de parcourir les applications déployées et de consulter leurs statistiques, comme la web-console, de consulter l'état des ressources, comme les datasources. 
Elle permet aussi d'accéder à certaines informations standard de la JVM et des composants JMX. 
Si EmbJOPR est plus joli que ses concurrents, *LE* point sur lequel il les surpasse est la modification de configuration: il est possible d'ajouter une datasource, par exemple, et de conserver cette modification après le prochain redémarrage. 
Les modifications de configuration de JBossAS deviennent persistantes, sans avoir à modifier soi-même les fichiers XML.


== Outils indépendants

=== jConsole

link:http://java.sun.com/developer/technicalArticles/J2SE/jconsole.html[JConsole] est un outil de monitoring intégré au JDK depuis la version 5. 
Il utilise en particulier le protocole JMX.

Pour qu'une application puisse être vue par jConsole, il faut activer sont interface JMX:

[source.width-80, subs="verbatim,quotes"]
----
JAVA_OPTS="-Dcom.sun.management.jmxremote"
----

Cette option permet un accès local; il est nécessaire avec le JDK 5, mais inutile à partir du JDK 6.

Pour qu'une application puisse être vue à distances, il faut préciser le port de communication:

[source.width-80, subs="verbatim,quotes"]
----
JAVA_OPTS="-Dcom.sun.management.jmxremote.port=9999"
----

Par défaut, la communication est sécurisée. Il est possible de préciser l'emplacement des fichiers d'authentification, ou de désactiver la sécurité (mode développement).

[source.width-80, subs="verbatim,quotes"]
----
 # Mode développement
 JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote.authenticate=false"
 JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote.ssl=false"
----


[source.width-80, subs="verbatim,quotes"]
----
 # Mode sécurisé
 JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote.password.file=jmxremote.password"
 JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote.access.file=jmxremote.access"
 JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.keyStore=tomboss.ks -Djavax.net.ssl.keyStorePassword=tompass"
----

Avec Tomcat, par exemple, cette technique permet d'accéder aux informations JMX de la machine virtuelle, ainsi qu'à celles du serveur d'application. 
Comme JBoss embarque son propre serveur JMX, les informations sont séparées. 
Depuis JBoss 4.0.3, il est possible de les rassembler, en ajoutant les options suivantes:

[source.width-80, subs="verbatim,quotes"]
----
 # Intégrer JMX de JBoss 4 et de JVM (à partir de JBoss 4.0.3)
 JAVA_OPTS="$JAVA_OPTS -Djboss.platform.mbeanserver -Djavax.management.builder.initial=org.jboss.system.server.jmx.MBeanServerBuilderImpl"
----

[source.width-80, subs="verbatim,quotes"]
----
 # Intégrer JMX de JBoss 5 et de JVM
 JAVA_OPTS="$JAVA_OPTS -Djboss.platform.mbeanserver"
----

Il faut noter, toutefois, que la combinaison de `-Djboss.platform.mbeanserver` et de `-Dcom.sun.management.jmxremote` fait planter JBoss 5 au démarrage! 
D'après le link:https://issues.redhat.com/browse/JBAS-6185[JIRA de JBoss], ce problème existe en version 5.0 et 5.1, mais est corrigé en 6.0. 
Pour les clients de RedHat ayant souscrit au support et utilisant EAP, le problème a été corrigé dans la version JBoss EAP 5.0.

=== Client JMX

L'application link:https://sourceforge.net/projects/mc4j/[MC4J] est plus nettement plus conviviale pour exploiter les informations des MBeans. 
Cependant, l'appel de certaines méthodes fonctionne mal car l'outil supporte mal le polymorphisme.

link:https://jmanage.blogspot.com/[jManage] est une solution intéressante car il s'installe en tant que serveur indépendant et peut surveiller plusieurs JBoss, Tomcat ou autres.

Je ne l'ai pas encore évalué, mais link:http://mbean-monitor.sourceforge.net/[MBeanMonitor] a l'air intéressant.

=== Nagios

L'application link:http://www.nagios.org/[Nagios] est fréquemment utilisée pour le suivi des environnements réseaux et serveurs. 
Les informations issues de MBeans de JBoss peuvent y être affichées. 
JBoss fournit la procédure pour installer un plug-in adapté à cette tâche dans Nagios. 
Vous trouverez cette information dans le link:http://www.jboss.org/community/docs/DOC-10676[wiki de JBoss].


== Twiddle

Twiddle est un outil en ligne de commande fourni avec JBoss (répertoire bin). Il permet d'accéder aux informations et aux opérations des MBeans déployés depuis la ligne de commande.


Pour des informations plus récentes que ce paragraphe, vous pouvez consulter le link:https://developer.jboss.org/docs/DOC-12458[wiki de jboss.org] (en anglais).

=== Options

* -s: nom ou adresse IP du serveur, avec éventuellement le port pour l'accès distant

[source.width-80, subs="verbatim,quotes"]
----
twiddle -s myserver ...   
twiddle -s myserver:1299 ...   
----

* -u, -p: authentification

[source.width-80, subs="verbatim,quotes"]
----
twiddle -u admin -p admin ... 
----

* -h: aide

[source.width-80, subs="verbatim,quotes"]
----
twiddle -h
----

* --help-commands: liste des commandes

[source.width-80, subs="verbatim,quotes"]
----
twiddle --help-commands
----

* -H: aide sur une commande

[source.width-80, subs="verbatim,quotes"]
----
twiddle -H ...
----

=== Liste des commandes

* info <mbean-name>: Get the metadata for an MBean
* get: Get the values of one or more MBean attributes
* invoke: Invoke an operation on an MBean
* create: Create an MBean
* setattrs: Set the values of one or more MBean attributes
* unregister: Unregister one or more MBeans
* query: Query the server for a list of matching MBeans
* set: Set the value of one MBean attribute
* serverinfo: Get information about the MBean server

=== Exemples

* `twiddle info "jboss.system:service=MainDeployer"`
* `twiddle query "jboss.system:*"`
* `twiddle serverinfo -l <=> twiddle query "*:*"`
* `twiddle get "jboss.system:type=ServerInfo"`
* `twiddle get "jboss.system:type=ServerInfo" FreeMemory`
* `twiddle invoke "jboss.system:service=MainDeployer" redeploy "file:///C:/mandeploy/hello.war/"`
* `twiddle invoke "jboss:service=JNDIView" list true`

L'exemple ci-dessous est valable pour JBoss 4.x.

* `twiddle invoke "jboss.deployment:flavor=URL,type=DeploymentScanner" scan`

En JBoss 5, pour avoir la même chose, il faut invoquer les 2 opérations suivantes:

* `twiddle invoke "jboss.deployment:flavor=URL,type=DeploymentScanner" stop`
* `twiddle invoke "jboss.deployment:flavor=URL,type=DeploymentScanner" start`

=== Problèmes

L'invocation de certaines opérations ou l'accès à certains attributs peuvent provoquer des exceptions `java.io.NotSerializableException`. 
Ceci peut être le cas lorsqu'on déclenche un scan. 

Ce problème ne se produit que dans certaines conditions. 
Par exemple, avec JBoss 4.0.5 et un JDK 5 de Sun. 
En revanche, le problème a été résolu pour la même version de JBoss avec un JDK 1.4. 
Un patch est disponible dans link:https://issues.redhat.com/browse/JBAS-1955[JIRA] et intégré dans la version 4.0.5.SP1 (uniquement dans SVN).

Le problème ne se produit pas dans JBoss 4.2.


== Accès depuis java

=== RMIAdaptor

L'appel depuis une classe Java peut se faire à distance (protocole RMI). 
Dans ce cas, il faut recherche l'adaptateur dans l'annuaire JNDI. 
Le lookup renvoie un objet de type javax.management.MBeanServerConnection à partir duquel on peut interroger des attributs de MBeans et invoquer des opérations.

[source.width-80, subs="verbatim,quotes"]
----
Context ctx = new InitialContext();
MBeanServerConnection server = (MBeanServerConnection) ctx
                               .lookup("jmx/invoker/RMIAdaptor");
Long freeMemory = (Long) server.getAttribute(new ObjectName(
                               "jboss.system:type=ServerInfo"),
                               "FreeMemory");
Long totalMemory = (Long) server.getAttribute(new ObjectName(
                               "jboss.system:type=ServerInfo"),
                               "TotalMemory"); 
System.out.println("Memoire utilisée="
                             + (totalMemory-freeMemory)/1024/1024
                             + "Mo");
server.invoke(new ObjectName(
                   "jboss.system:service=MainDeployer"),
               "redeploy", 
               new Object[] {"file:///C:/mandeploy/hello.war/"}	,
               new String[] {"java.lang.String"});
----

=== MBeanServerLocator

L'appel depuis une classe Java peut se faire en local, depuis une JSP, une servlet ou un EJB, par exemple :

[source.width-80, subs="verbatim,quotes"]
----
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java"
         import="javax.management.*,org.jboss.mx.util.*"%>

<%
   MBeanServer server = MBeanServerLocator.locateJBoss();

   long freeMemory = (Long) server.getAttribute(new ObjectName(
                        "jboss.system:type=ServerInfo"),
                        "FreeMemory");
   long totalMemory = (Long) server.getAttribute(new ObjectName(
                        "jboss.system:type=ServerInfo"),
                        "TotalMemory");
%>

<HTML>
<HEAD>
  <TITLE>Hello JSP</TITLE>
</HEAD>

<BODY>
  Memoire utilisée=
  <%=(totalMemory - freeMemory) / 1024 / 1024%>
  Mo
</BODY>
</HTML>
----

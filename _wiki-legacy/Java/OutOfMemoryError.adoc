---
title: Java/OutOfMemoryError
tags: ''
toc: true
created: '2012-04-28'
modified: '2012-04-28'
---
En cours de rédaction...


== hotspot


* Managed Heap
* Perm Space
* Thread : unable to create new native thread
* Direct ByteBuffer : Allocated 1953546760 bytes of native memory before running out

== j9


* Managed Heap
* Thread : Failed to fork OS thread
* Direct ByteBuffer : Unable to allocate 1048576 bytes of direct memory after 5 retries

== Références

* link:http://www.ibm.com/developerworks/linux/library/j-nativememory-linux/[http://www.ibm.com/developerworks/linux/library/j-nativememory-linux/]


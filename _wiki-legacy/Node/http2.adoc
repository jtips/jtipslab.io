---
title: HTTP/2 avec Node.js
tags: JavaScript HTTP2
toc: true
archive: false
author: Alexis
created: '2017-10-01'
modified: '2017-10-01'
---



Le support d'HTTP/2 a été introduit avec Node.js 8.x. 
Je ne sais exactement depuis quelle version mineure, en tout cas avec *Node.js 8.6*, il est présent et marqué comme *expérimental*.


Le module http2 devrait être stable pour Node.js 9.


Pour les versions précédentes, le support d'HTTP/2 peut exister grâce à des link:https://github.com/molnarg/node-http2[modules tiers].


== HTTP/2 par ALPN


[source.width-80, subs="verbatim,quotes"]
----
const http2 = require('http2');
const fs = require('fs');

const server = http2
  .createSecureServer(
    {
      key: fs.readFileSync('resources/node.key'),
      cert: fs.readFileSync('resources/node.crt')
    },
    (request, response) => {
      response.end('Hello World');
    }
  )
  .listen(8002);

----

== HTTP/2 avec Express.js

En théorie, ça devrait fonctionner avec le bout de code ci-dessous.



[source.width-80, subs="verbatim,quotes"]
----
const http2 = require('http2');
const fs = require('fs');

----


[source.width-80, subs="verbatim,quotes"]
----
const app = express();
app.get('*', (req, res) => {
  res
    .status(200)
    .json({message: 'Hello'})
});

const server = http2
  .createSecureServer(
    {
      key: fs.readFileSync('resources/node.key'),
      cert: fs.readFileSync('resources/node.crt')
    },
    app
  )
  .listen(8002);

----

Mais, avec Node.js 8. et Express.js 4.16, ça ne fonctionne pas, il y a une erreur à la première requête.


Il semble qu'il faille link:https://github.com/expressjs/express/pull/3390[attendre la version 5 d'Express.js].
C'est un échec avec Express.js 5.0.0-alpha.6 







|}



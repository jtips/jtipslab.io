---
title: Administration JBoss AS 7 en ligne de commande
tags: JBoss7
toc: true
redirects:
- /WildFly/cli-legacy
links:
- url: https://www.sewatech.fr/formation-wildfly.html
  text: Administration WildFly
author: Alexis
created: 2021-12-01
modified: 2021-12-01
---

WARNING: Certaines commandes sont trop longues pour être affichées sur une seule ligne ; dans ce cas, je les ai affichées sur plusieurs lignes, avec '\' comme signe de continuation.

== JBoss AS 7 / EAP 6

La plupart des commandes sont identiques entre JBoss et WildFly. Il y a quand même quelques commandes qui sont différentes.

Les commandes décrites ici concernent les anciennes versions de WildFly que sont *JBoss AS 7* et son dérivé commercial *JBoss EAP 6*.
Pour WildFly, les lignes de commandes sont dans la page link:/WildFly/CLI[Administration WildFly en ligne de commande].

=== Access Log

Pour *JBoss AS 7*, le subsystem undertow n'existait pas, son équivalent était le subsytem web, et la configuration était légèrement différente :

[source.width-80, subs="verbatim,quotes"]
----
[host:9990 /] /subsystem=web/virtual-server=default-host/access-log=configuration:add

[host:9990 /] cd /subsystem=web/virtual-server=default-host/access-log=configuration
[host:9990 /] :write-attribute(name=pattern, value=%h\ %l\ %u\ %t\ "%r"\ %s\ %b\ %D)
----

Dans *JBoss EAP 6*, on a aussi le subsystem web, mais la commande est légèrement différente :

[source.width-80, subs="verbatim,quotes"]
----
[host:9990 /] /subsystem=web/virtual-server=default-host/configuration=access-log:add

[host:9990 /] cd /subsystem=web/virtual-server=default-host/configuration=access-log
[host:9990 /] :write-attribute(name=pattern, value="%h %l %u %t \"%r\" %s %b %D")
----

=== Authentification

J'ai mis un peu de temps pour comprendre comment construire cette commande pour *JBoss AS 7*, mais la voilà, pour un security domain qui utilise une *base de données*.

[source.width-80, subs="verbatim,quotes"]
----
[host:9990 /] /subsystem=security/security-domain=sw-domain:add(cache-type=default)
[host:9990 /] /subsystem=security/security-domain=sw-domain/authentication=classic                                \
                  :add(login-modules=[                                                                            \
                        {"code"=>"Database", "flag"=>"required",                                                  \
                         "module-options"=>{"dsJndiName" => "java:/datasources/SewaDS",                           \
                                            "principalsQuery" => "select pwd from Users where username=?",        \
                                            "rolesQuery" => "select roles, 'Roles' from Roles where username=?"}}])
----

Et pour un security domain qui utilise des *fichiers propoerties*, ça ressemble beaucoup.

[source.width-80, subs="verbatim,quotes"]
----
[host:9990 /] /subsystem=security/security-domain=sw-domain:add(cache-type=default)
[host:9990 /] /subsystem=security/security-domain=sw-domain/authentication=classic                                \
                   :add(login-modules=[                                                                           \
                        {"code"=>"UsersRoles", "flag"=>"required",                                                \
                         "module-options"=>{"usersProperties"=>"${jboss.server.config.dir}/sw-users.properties",  \
                                            "rolesProperties"=>"${jboss.server.config.dir}/sw-roles.properties"}}])
----

La configuration de JBoss EAP 6 est similaire à WildFly.

=== Développement

Dans JBoss AS 7, la commande était légèrement différente, puisqu'on avait un sous-système web à la place de undertow :

[source.width-80, subs="verbatim,quotes"]
----
[host:9990 /] /subsystem=web/configuration=jsp-configuration:write-attribute(name=development, value=true)
----

=== Communications SSL

Après avoir créé la paire de clés, on crée un connector sur le port 8443, qui utilise la clé (placée dans le répertoire configuration de mon WildFly).

[source.width-60, subs="verbatim,quotes"]
----
[host:9990 /] /subsystem=web/connector=https:add                               \
                     (protocol=HTTP/1.1,                                       \
                      scheme=https,                                            \
                      secure=true,                                             \
                      socket-binding=https)
[host:9990 /] /subsystem=web/connector=https/ssl=configuration:add             \
                     (certificate-key-file=${jboss.server.config.dir}/sewa.ks, \
                      key-alias=sewa,                                          \
                      password=sewakey,                                        \
                      protocol=TLS)
----

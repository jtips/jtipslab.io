---
title: Liste des timezones en Javascript
tags: 
- JavaScript 
toc: true
created: '2021-05-25'
modified: '2021-05-25'
author: Alexis
---

== En Java

OK, c'est hors sujet, mais si on admet pouvoir gérer cette liste coté backend, en java, c'est assez simple :

[source, subs="verbatim,quotes"]
----
Set<String> timezoneIds = ZoneId.getAvailableZoneIds();
----

Au moment de la rédaction de la page, l'ensemble est constitué de 600 zones.

Les ids sont les codes IANA (UTC, Europe/Paris, Antarctica/Troll,...).

== Avec Moment.JS

Il faut ajouter le modules moment-timezone.

[source, subs="verbatim,quotes"]
----
timezoneIds = moment.tz.names();
----

Comme pour la variante Java, on récupère une liste de codes IANA, au nombre de 593.
En réalité, il y a 19 différences entre les deux listes.

Cette solution fonctionne bien, mais elle a 2 défauts :

* Moment.JS est en fin de vie.
* La définition complète des fuseaux horaires aloudri beaucoup l'application.

== Pur JavaScript

La liste des fuseaux horaires peut varier d'une implémentation à l'autre.
Le seul fuseau qui doit obligatoirement être supporté est UTC.

Généralement, les codes de la base IANA sont supportés.
Malheureusement, il n'y a pas d'API standard pour récupérer une telle liste.

---
title: Profils avec Quarkus
tags: Quarkus
toc: true
importance: 2
author: Alexis
created: 2023-06-30
modified: 2021-12-24
---

La notion de _profile_ ressemble à celle de Spring Framework, avec quelques différences inhérentes au fonctionnement de Quarkus.

Malgré les différences, on peut reprendre le même exemple courant avec la _datasource_ qui peut être différente entre les environnements de développement, de test et de déploiement.


== Configuration

Propriété système quarkus.profile

[source, subs="verbatim,quotes"]
----
java -Dquarkus.profile=stage org.example.Main
----

En properties ou yaml, préfixe %dev.
En variable d'environnement, préfixe _DEV_

Profils prédéfinis : dev, prod, test

== Prog

ConfigUtils.getProfiles()

On trouve encore `ProfileManager.getLaunchMode()`, mais il est déprécié.

== Profils par défaut

Il dépend du mode de démarrage.


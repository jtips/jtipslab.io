---
title: Protéger les mots de passe de WildFly dans un caveau
tags: WildFly
toc: true
redirects:
- /WildFly/vault
links:
- url: https://www.sewatech.fr/formation-wildfly.html
  text: Administration WildFly
author: Alexis
created: 2013-07-24
modified: 2021-12-08
---

La technique de Vault permet de ne plus afficher les mots de passe, ou autres chaînes de caractères sensibles, dans les fichiers de configuration de WildFly.

Cette fonctionnalité a été introduite en *JBoss AS 7.1*, puis a été abandonnée dans elytron. 
De ce fait, elle n'est plus utilisable dans *WildFly 25*.

Les commandes décrites dans cette page ont été testées avec *OpenJDK 11* et *WildFly 23*.


== Mise en oeuvre

=== keystore

Pour créer le keystore, on utilise l'outil keytool fourni avec le JDK. 
On crée le magasin et on y inclut directement l'alias vault qu'on utilisera plus tard.

[source.width-80, subs="verbatim,quotes"]
----
~# keytool -genseckey -storetype jceks -keyalg AES -keysize 128               \
           -keystore standalone/configuration/vault.ks -storepass vaultpwd    \
           -alias vault -keypass vaultpwd
----

=== vault

Le script `vault.sh` peut être utilisé de façon interactive ou avec des paramètres. 
Le mode non interactif, avec paramètres, n'est pas documenté. 
Mais `vault.sh --help` apporte les informations nécessaires.

Pour initialiser notre caveau, il faut indiquer 

* les informations sur le keystore (fichier, alias du caveau, mot de passe), 
* le répertoire qui contiendra les fichiers chiffrés, 
* les informations de salage du mot de passe (chaine et nombre d'itérations). 

On y stocke directement des informations sensibles, avec le bloc, le nom et la valeur de l'attribut.

[source.width-80, subs="verbatim,quotes"]
----
~# bin/vault.sh --keystore standalone/configuration/vault.ks                  \
                --keystore-password vaultpwd                                  \
                --alias vault --enc-dir standalone/data/vault                 \
                --iteration 12 --salt azertyui                                \
                --vault-block SewaDS --attribute password --sec-attr sapwd
----

Le première parte du résultat est le contenu du caveau, avec la chaine à utiliser à la place du mot de passe :

[source.width-80, subs="verbatim"]
----
********************************************
Vault Block:SewaDS
Attribute Name:password
Configuration should be done as follows:
VAULT::SewaDS::password::1
********************************************
----

La seconde partie du résultat est la commande CLI à passer.

=== configuration

Cette configuration de caveau peut maintenant être intégrée.

[source.width-80, subs="verbatim,quotes"]
----
[9990 /] /core-service=vault                                                  \
            :add(vault-options=                                               \
                    [("KEYSTORE_URL"=>"${jboss.server.config.dir}/vault.ks"), \
                     ("KEYSTORE_PASSWORD"=>"MASK-3TYfSWtwSqg4vC2Y56enLw"),    \
                     ("KEYSTORE_ALIAS"=>"vault"),                             \
                     ("SALT"=>"azertyui"),                                    \
                     ("ITERATION_COUNT"=>"12"),                               \
                     ("ENC_FILE_DIR"=>"${jboss.server.data.dir}/vault/")])
----

=== utilisation

On peut maintenant utiliser le mot de passe stocké dans le caveau avec la chaine générée, qui commence par VAULT. 
Dans mon cas, je voulais l'utiliser pour une datasource dont la configuration ressemblait à ça :

[source.width-80, subs="verbatim,quotes"]
----
<datasource ...>
  ...
  <security>
    <user-name>sa</user-name>
    <password>sa</password>
  </security>
</datasource>
----

La nouvelle configuration devient :

[source.width-80, subs="verbatim,quotes"]
----
<datasource ...>
  ...
  <security>
    <user-name>sa</user-name>
    <password>${VAULT::SewaDS::password::1}</password>
  </security>
</datasource>
----

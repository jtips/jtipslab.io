---
title: HTTP/2 avec Nginx
tags: Nginx HTTP2
toc: false
author: Alexis
created: 2024-06-15
modified: 2024-06-15
---

Avant de commencer, il faut vérifier si notre nginx supporte HTTP/2.
En effet, pour avoir le module `ngx_http_v2_module` il faut que nginx soit compilé avec l'option `--with-http_v2_module`.

[source.width-80, subs="verbatim,quotes"]
----
~$ nginx -V

configure arguments: --with-cc-opt='-g -O2 -ffile-prefix-map=/build/nginx-zctdR4/nginx-1.18.0=. -flto=auto -ffat-lto-objects -flto=auto -ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security -fPIC -Wdate-time -D_FORTIFY_SOURCE=2' --with-ld-opt='-Wl,-Bsymbolic-functions -flto=auto -ffat-lto-objects -flto=auto -Wl,-z,relro -Wl,-z,now -fPIC' --prefix=/usr/share/nginx --conf-path=/etc/nginx/nginx.conf --http-log-path=/var/log/nginx/access.log --error-log-path=/var/log/nginx/error.log --lock-path=/var/lock/nginx.lock --pid-path=/run/nginx.pid --modules-path=/usr/lib/nginx/modules --http-client-body-temp-path=/var/lib/nginx/body --http-fastcgi-temp-path=/var/lib/nginx/fastcgi --http-proxy-temp-path=/var/lib/nginx/proxy --http-scgi-temp-path=/var/lib/nginx/scgi --http-uwsgi-temp-path=/var/lib/nginx/uwsgi --with-compat --with-debug --with-pcre-jit --with-http_ssl_module --with-http_stub_status_module --with-http_realip_module --with-http_auth_request_module **--with-http_v2_module **--with-http_dav_module --with-http_slice_module --with-threads --add-dynamic-module=/build/nginx-zctdR4/nginx-1.18.0/debian/modules/http-geoip2 --with-http_addition_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_sub_module
----

== h2

La façon d'activer HTTP/2 dépend de la version de nginx.
Dans les anciennes versions, c'est une option de la directive `listen`.

[source.width-80, subs="verbatim,quotes"]
----
server {
  listen 443 ssl **http2**;
  ...
}
----

Depuis la version 1.25.1, c'est devenu une directive.

[source.width-80, subs="verbatim,quotes"]
----
server {
  listen 443 ssl;

  **http2 on;**
  ...
}
----

== h2c

La directive `http2` active le protocole sur tous les ports du server, y compris les ports sans TLS, comme 80.
Comme les navigateurs ne supportent pas h2c, il faut un autre outil, comme curl avec l'option --http2 car l'upgrade n'est pas automatique.

[source.width-80, subs="verbatim,quotes"]
----
~$ curl **--http2** http://localhost
----

Il faut noter qu'avant la directive, nginx supportait h2c, mais sans l'upgrade HTTP/1 !> HTTP/2.
Il fallait donc l'activer sur un port séparé.

[source.width-80, subs="verbatim,quotes"]
----
server {
  listen 80 default_server;
  listen 81 http2;
  ...
----

Pour tester avec curl, il fallait bien demander HTTP/2 directement.

[source.width-80, subs="verbatim,quotes"]
----
~$ curl **--http2-prior-knowledge** http://localhost:81
----

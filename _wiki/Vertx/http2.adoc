---
title: HTTP/2 avec Vert.x
tags: Vertx HTTP2
toc: true
importance: 1
links:
- url: https://www.sewatech.fr/formation-vertx.html
  text: 'Développement Web avec Vert.X '
author: Alexis
created: '2017-09-30'
modified: '2017-09-30'
---

== Support d'ALPN

Pour Vert.X 3.3 et 3.4

* Avec l'extension ALPN de Jetty (en Java 8)
* Avec OpenSSL

== Code

Pour activer HTTP/2, il faut d'abord un serveur HTTP avec TLS, sur lequel on active ALPN.

[source.width-80, subs="verbatim,quotes"]
----
HttpServerOptions h2Options = new HttpServerOptions()
               .setSsl(true)
               .setKeyStoreOptions(
                       new JksOptions()
                               .setValue(Buffer.buffer(getKeyStore()))
                               .setPassword(KEYSTORE_PASSWORD))
               .setUseAlpn(true);
----

== OpenSSL

Le code ci-dessus produit une erreur si on utilise *JSSE*, l'implémentation Java de TLS.
Par contre, ça fonctionne si on passe en *OpenSSL*.
Et pour ça, il faut ajouter l'extension *BoringSSL* de Netty.

[source.width-80, subs="verbatim,quotes"]
----
<dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-tcnative-boringssl-static</artifactId>
    <version>1.1.33.Fork26</version>
    <scope>runtime</scope>
</dependency>
----

Remarque : ça fonctionne avec Vert.x 3.4 (donc Netty 4.1) + Netty tcnative 1.1, par contre, ça ne marche pas avec Netty tcnative 2.0.

== JSSE

Comme pour Tomcat, HTTP/2 peut fonctionner en *JSSE* avec Java 8 à condition d'ajouter l'extension ALPN de Jetty au bootclasspath.

[source.width-80, subs="verbatim,quotes"]
----
java -Xbootclasspath/p:alpn-boot-8.1.11.v20170118.jar ...
----

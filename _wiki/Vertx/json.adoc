---
title: JSON dans Vert.X
tags: Vertx
toc: true
importance: 1
links:
- url: https://www.sewatech.fr/formation-vertx.html
  text: 'Développement Web avec Vert.X '
author: Alexis
created: '2017-05-15'
modified: '2017-05-15'
---

*Vert.x* core fournit quelques classes pour faire du JSON.
Il les utilise dans ses APIs.
C'est https://github.com/FasterXML/jackson[Jackson] qui est utilisé de façon cachée.

== Json

La classe `io.vertx.core.json.Json` est essentiellement un wrapper pour ObjectMapper de Jackson.
Elle est là pour nous cacher et simplifier l'utilisation de l'ObjectMapper, avec des méthodes statiques.

Pour passer d'un contenu json à un objet Java :

[source.width-80, subs="verbatim,quotes"]
----
String jsonString = "{\"name\":\"Alexis\"}";
Person person = Json.decodeValue(jsonString, Person.class);
----

Il existe aussi une version joliment formattée.

Et pour le sens inverse :

[source.width-80, subs="verbatim,quotes"]
----
String jsonPerson = Json.encode(person);
----

== JsonObject

La classe `io.vertx.core.json.JsonObject` permet de charger du contenu json dans un objet non typé, qui ressemble à une map.

[source.width-80, subs="verbatim,quotes"]
----
JsonObject jsonObject = new JsonObject(jsonString);
String name = jsonObject.getString("name");
----

On a donc des méthodes `getXxx()` pour récupérer les valeurs et des méthodes `put()` pour en modifier ou en ajouter.

Et pour transformer en json, on `encode()`, avec aussi une variante _prettily_ :

[source.width-80, subs="verbatim,quotes"]
----
jsonObject.put("Age", 42);
String result = jsonObject.encode());
----

== JsonArray

La classe `io.vertx.core.json.JsonArray` est l'équivalent de JsonObject pour des tableaux.

[source.width-80, subs="verbatim,quotes"]
----
String jsonArrayString = "[\"Alexis\", \"Alice\"]";

JsonArray jsonArray = new JsonArray(jsonArrayString);
String first = jsonArray.getString(0));

jsonArray.add("Bob");
String result = jsonArray.encode());
----

---
title: API Filter d'Hibernate
tags: Hibernate
toc: true
importance: 1
author: Olivier
created: '2008-10-30'
modified: '2009-01-15'
---

== Filtrer une association de type many

Hibernate donne la possibilité de filtrer des associations de type collection. Cette alternative aux requêtes HQL classiques permet notamment de contourner certains problèmes de requêtage dûs aux associations unidirectionnelles. Prenons l'exemple d'une catégorie associée à un ensemble de produits. L'association est unidirectionnelle, i.e. une catégorie "voit" les produits auxquels elle est associée, mais les produits ne "voient" pas la catégorie.

[source.width-80, subs="verbatim,quotes"]
----
public class Categorie implements Serializable {
  private Long id;
  private String code;
  private String libelle;
  private Set<Produit> produits;
  
  // getters et setters
  ...
}
public class Produit implements Serializable {
  private Long id;
  private String libelle;
  private float prix;
  
  // getters et setters
  ...
}
----

Avec ce type d'association, il est impossible de lire les produits d'une catégorie via la requête HQL suivante puisque la propriété _categorie_ n'existe pas dans la classe _Produit_ :

[source.width-80, subs="verbatim,quotes"]
----
 String hql = "from Produit p where p.categorie = :cat";
 Query query = session.createQuery(hql);
 List resultats = query.list(); => ECHEC
----

En revanche, nous pouvons utiliser un filtre de collection (méthode _createFilter()_) pour obtenir le résultat souhaité :

[source.width-80, subs="verbatim,quotes"]
----
 Categorie cat = (Categorie)session.load(Categorie.class, new Long(1));
 Query  query = session.createFilter(cat.getProduits(), "");
 List<Produit> produits = query.list();
 for (Produit produit : produits) {
   System.out.println("Produit: code=" + produit.getCode() + ",titre=" + produit.getTitre());
 }
----

== Appliquer des restrictions au filtre

Le deuxième paramètre de la méthode _createFilter()_ permet d'ajouter des restrictions à la requête générée. Ces restrictions sont exprimées sous la forme de fragment de HQL :

[source.width-80, subs="verbatim,quotes"]
----
 Categorie cat = (Categorie)session.load(Categorie.class, new Long(1));
 Query  query = session.createFilter(cat.getProduits(), "where this.prix > :par_prix")
                       .setFloat("par_prix", 40f);
 List<Produit> produits = query.list();
 for (Produit produit : produits) {
   System.out.println("Produit: code=" + produit.getCode() + ",titre=" + produit.getTitre());
 }
----

Remarque : le mot clé _this_ permet de faire référence à l'instance de produit en cours.


== La clause select

La requête peut retourner une sélection de propriétés grâce à la clause _select_ :

[source.width-80, subs="verbatim,quotes"]
----
 Categorie cat = (Categorie)session.load(Categorie.class, new Long(1));
 Query  query = session.createFilter(cat.getProduits(), "select this.titre where this.prix > :par_prix")
                       .setFloat("par_prix", 40f);
 List<String> titres = query.list();
 for (String titre : titres) {
   System.out.println("Titre produit: " + titre);
 }
----

Remarque : la clause _from_ n'est pas nécessaire.

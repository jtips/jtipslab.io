---
title: Commandes pratiques pour Docker
short-title: Commandes Docker
tags: Docker
toc: true
importance: 1
links:
- url: https://www.sewatech.fr/formation-docker-developpeurs.html
  text:  Docker pour les développeurs
author: Alexis
created: '2014-08-10'
modified: '2021-11-29'
---

Cette page reprend quelques commandes Docker plutôt usuelles qui ne se trouvent pas directement dans la documentation officielle.
// <!--more-->

== Quelques commandes un peu plus élaborée

=== Supprimer les images

Supprimer toutes les images non utilisées :

[source, subs="verbatim,quotes"]
----
docker rmi $(docker images -q)
----

Supprimer toutes les images sans tag :

[source, subs="verbatim,quotes"]
----
docker rmi $(docker images | grep "<none>" | awk '{print $3}')
----

=== Démarrer un conteneur avec cette image, en mode interactif

[source, subs="verbatim,quotes"]
----
docker run -ti hasalex/img bash
----

L'option -t permet de détacher le conteneur avec ^P^Q, sans l'arrêter. On pourra le rattacher ensuite avec `docker attach`.

=== Arrêter tous les conteneurs

[source, subs="verbatim,quotes"]
----
docker stop $(docker ps -q)
----

=== Supprimer tous les conteneurs

En réalité, ça ne supprime que ceux qui sont arrêtés.

[source, subs="verbatim,quotes"]
----
docker rm $(docker ps -a -q)
----

Cette commande peut être combinée avec celle d'arrêt pour arrêter et supprimer tous les conteneurs :

[source, subs="verbatim,quotes"]
----
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
----

=== Adresse IP d'un conteneur

Pour avoir l'adresse IP du conteneur 000000 :

[source, subs="verbatim,quotes"]
----
docker inspect --format '{{ .NetworkSettings.IPAddress }}' 000000
----

Ça peut se coupler avec un `kill` pour envoyer un signal au process :

[source, subs="verbatim,quotes"]
----
kill -INT $(docker inspect --format '{{ .NetworkSettings.IPAddress }}' 000000)
----

Ceci dit, ça peut être fait plus simplement :

[source, subs="verbatim,quotes"]
----
docker kill --signal=INT 000000
----

=== PID du conteneur

Pour avoir l'ID du process dans le host :

[source, subs="verbatim,quotes"]
----
docker inspect --format '{{.State.Pid}}' 000000
----

== Docker Hub

[source, subs="verbatim,quotes"]
----
docker login
----

Envoyer une image sur le hub :

[source, subs="verbatim,quotes"]
----
docker push hasalex/img
----

== Références

* https://docs.docker.com/reference/cli/docker/[The base command for the Docker CLI]
* https://docs.docker.com/reference/dockerfile/[Dockerfile reference]
* https://docs.docker.com/docker-hub/[Docker Hub quickstart]

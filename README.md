This is the source of the [Jtips wiki](https://jtips.info):

* developed with [Jekyll](https://jekyllrb.com/),
* built with a [Gitlab pipeline](https://docs.gitlab.com/ee/ci/pipelines/),
* deployed on [Gitlab pages] (https://docs.gitlab.com/ee/user/project/pages/) with a custom domain name.

Jekyll::Hooks.register :site, :post_read do |site|
    user_pages = site.collections
        .select { |key, collection| key.start_with?('special') }
        .map { |key, collection| collection.docs }
        .flatten
        .select { |page| page.data['layout'] == 'user' }

    wiki_pages = site.collections
        .select { |key, collection| key.start_with?('wiki') }
        .map { |key, collection| collection.docs }
        .flatten
    site.data['wiki_pages'] = wiki_pages

    wiki_tags = wiki_pages
        .map { |page| page.data['tags'] }
        .flatten
        .uniq
    site.data['wiki_tags'] = wiki_tags

    prefix = site.data['i18n'][site.config['lang']]['prefix']['user']
    authors = site.data['users'].map do |user|
        contributions = wiki_pages.select { |page| page.data['author'] == user[0] }
        author = {'id' => user[0], 
                  'name' => user[1]['name'], 
                  'logo' => user[1]['logo'], 
                  'url' => "/#{prefix}#{user[0]}",
                  'contributions' => contributions,
                  'contributions_count' => contributions.length,
                  'contributions_url' => "/#{prefix}contributions/#{user[0]}"
                }
        user_pages.select { |page| page.url == author['url'] }
                  .each { |page| page.data['author_full'] = author }
        author
    end
    site.data['authors'] = authors

    wiki_pages.each do |page|
        page.data['author_full'] = authors.select { |author| author['id'] == page.data['author'] }.first
        if !page.data['description'] then
            excerpt = /<p>(.*?)<\/p>/mi.match(page.data['excerpt'].output)
            excerpt = strip_html(excerpt).gsub("\n ", ' ').gsub("\n", ' ').strip()

            if !excerpt.empty? then
                page.data['description'] = shorten(excerpt, 160)
            end
        end
    end
end

Jekyll::Hooks.register :documents, :post_render do |doc|
    if doc.content then
        doc.data['size']=doc.content.split(' ').size
        doc.data['bytes']=doc.content.bytesize
    else
        doc.data['size']=0
        doc.data['bytes']=0
    end
end

def strip_html(input)
    Class.new.extend(Liquid::StandardFilters).strip_html(input)
end 

def shorten(text, size)
    text = CGI.unescapeHTML(text)
    if (text.size > size) then
        text = text[0..size + 1]
        last_point_index = text.rindex('. ')
        if (last_point_index && last_point_index > size * 3 / 4)  then
            CGI.escapeHTML(text[0..last_point_index])
        else
            last_word_index = text[0..size - 3].rindex(' ') - 1
            CGI.escapeHTML(text[0..last_word_index] + '...')
        end
    else 
        CGI.escapeHTML(text)
    end
end 
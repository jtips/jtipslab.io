---
layout: simple
title: Alexis Hassler
created: 2007-02-07
modified: 2021-12-24
---

[.floatleft]
image::/img/auteur/alexis.png[Photo de Alexis Hassler, 120, 120]

[.floatright]
image::/img/auteur/sewatech.png[Logo de Sewatech, 142, 81]

*Alexis Hassler* est développeur, architecte et formateur en technologies Java, depuis une vingtaine d'années. 
Il intervient auprès de ses clients pendant les phases de lancement de projet, pour définir la méthode et l'outillage nécessaires à l'équipe, puis dans la définition des architectures techniques. 
Enfin, il suit les projets en support lors du développement et pour la mise en production.

[.clear]
Ses principaux domaines de compétences sont les suivants:

* Architecture

* Développement java
** Spring Framework / Spring Boot, Hibernate, Jakarta EE / Java EE, JUnit
** IntelliJ, Eclipse

* Serveur d'application / Web
** JBoss EAP, WildFly, Apache Tomcat
** Apache HTTP Server, nginx

[.mt-1]
Alexis est le fondateur de https://www.sewatech.fr[SEWATECH, cabinet de conseil et formation]. 
Retrouvez le https://www.sewatech.fr/formations[catalogue de formations] sur le site de la société.

Alexis a été un des fondateurs du https://lyonjug.org/[Lyon JUG], le Java User Group de Lyon et de sa région, et à participé à l'organisation des premières éditions de la conférence https://mixitconf.org[Mix-IT].

Vous pouvez le contacter à l'adresse "alexis chez jtips.info" pour toute question ou remarque concernant le site en général et ses contributions.

[.mt-1]
Liens:

* https://blog.alexis-hassler.com[Blog], https://www.alexis-hassler.com/[CV et présentation]
* https://piaille.fr/@AlexisHassler[Mastodon], https://bsky.app/profile/alexis-hassler.com[Bluesky]
* https://github.com/hasalex/[Github], https://gitlab.com/hasalex/[Gitlab]
* https://www.linkedin.com/in/hassler[LinkedIn]
